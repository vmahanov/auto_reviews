ActiveAdmin.register Catalog do
  permit_params :title, :description
  actions :index, :show, :update, :edit

  filter :title
  filter :description

  index do
    id_column
    column :href_id
    column :title
    column (:description) { |r| r.description.html_safe }
    actions
  end

  form do |f|
    inputs '' do
      input :title
      input :description, as: :ckeditor
    end
    actions
  end

end
