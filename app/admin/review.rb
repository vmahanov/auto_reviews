ActiveAdmin.register Review do
  permit_params :name, :city, :email, :auto_show_id, :description, :value, :photo
  menu label: "Отзывы", priority: 4
end
