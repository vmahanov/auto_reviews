ActiveAdmin.register Brand do
  permit_params :name, :href_id
  menu priority: 2

  filter :name
  filter :href_id

  index do
    id_column
    column :name
    column :href_id
    actions
  end

  form do |f|
    inputs '' do
      input :name
      input :href_id
    end
    actions
  end
end
