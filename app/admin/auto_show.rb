ActiveAdmin.register AutoShow do
  permit_params :name, :address, :work_time, :phone, :site, :popular, :logo, :picture, :services, :variants, :description, :href_id,
                :email, :top_header, :top_text,
                brands: []
  menu priority: 3

  filter :name
  filter :href_id
  filter :address
  filter :work_time
  filter :phone
  filter :site
  filter :email
  filter :popular
  filter :brands_raw
  filter :services
  filter :variants
  filter :description


  index do
    id_column
    column :name
    column :href_id
    column :address
    column :work_time
    column :phone
    column :site
    column :email
    column :popular
    column :brands_names
    actions
  end

  form do |f|
    inputs '' do
      f.input :name
      f.input :href_id
      f.input :popular
      f.input :address
      f.input :work_time
      f.input :phone
      f.input :site
      f.input :email
      f.input :brands, as: :select, collection: options_for_select(Brand.all.map{|i| [i.name, i.id]}, auto_show.brands),
              multiple: true, input_html: {size: 10}
      f.input :logo
      f.input :picture
      f.input :services, as: :ckeditor
      f.input :variants, as: :ckeditor
      f.input :description, as: :ckeditor
    end
    inputs 'Верхний блок' do
      f.input :top_header
      f.input :top_text, as: :ckeditor
    end
    actions
  end

  show do
    tabs do
      tab  :basic do
        columns do
          column max_width: "350px", min_width: "100px" do
            div do
              image_tag auto_show.logo, style: "width: 50%;"
            end
            div do
              image_tag auto_show.picture, style: "width: 100%;"
            end
          end
          column do
            attributes_table_for auto_show, :name, :href_id, :popular, :address, :work_time, :phone, :site, :brands_names
          end
        end
      end
      tab :top_text do
        attributes_table_for auto_show, :top_header
        div do
          auto_show.top_text.html_safe
        end
      end
      tab  :services do
        auto_show.services.html_safe
      end
      tab  :variants do
        auto_show.variants.html_safe
      end
      tab  :description do
        auto_show.description.html_safe
      end
    end
  end

end
