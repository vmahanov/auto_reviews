class AutoShow < ApplicationRecord
  mount_uploader :logo, LogoUploader
  mount_uploader :picture, PictureUploader

  serialize :brands, Array
  attr_accessor   :brands_raw

  has_many :reviews

  def brands_raw
    self.brands.join("\n") unless self.brands.blank?
  end

  def brands_raw=(values)
    self.brands = []
    self.brands=values.split("\n")
  end

  def brands_names_row
    Brand.select(:name).where(id: brands).map{|b| b.name}
  end

  def brands_obj
    Brand.where(id: brands)
  end

  def brands_names
    brands_names_row.join(', ')
  end

  def address_for_google
    address.gsub(/([.,:])\s/,'\1').gsub(/\s/,'+')
  end

  scope :best, -> {
    limit(9)
        .where('(select count(*) from reviews where auto_show_id = auto_shows.id)>0')
        .order('(select avg(value) from reviews where auto_show_id = auto_shows.id) DESC')
  }
  scope :worst,  -> {
    limit(9)
        .where('(select count(*) from reviews where auto_show_id = auto_shows.id)>0')
        .order('(select avg(value) from reviews where auto_show_id = auto_shows.id) ASC')
  }
  scope :discussed, lambda {limit(9).order('(select count(*) from reviews where auto_show_id = auto_shows.id) DESC')}
  scope :brand,     lambda {|brand_id| where("brands like '%#{brand_id}%'")
                                          .order('(select avg(value) from reviews where auto_show_id = auto_shows.id) DESC')}
  scope :ordered,   lambda {order(:name)}

  def rating
    self.reviews.average('value').to_f.round
  end
end
