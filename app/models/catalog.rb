class Catalog < ApplicationRecord
  def title_for_brand(brand)
    title.gsub('%{brand}', brand)
  end
end
