class Review < ApplicationRecord
  belongs_to :auto_show
  mount_uploader :photo, PhotoUploader

  scope :last_registered, -> {where 'created_at > ?', Time.now - (86400 * 7) }
  scope :ordered, -> {order :created_at}
end
