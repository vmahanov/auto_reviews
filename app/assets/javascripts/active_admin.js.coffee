#= require active_admin/base
#= require 'ckeditor/init'

# There are some problems with ckeditor working with the float: left css property set on the formtastic labels.
# To quickly bypass this you can remove the float from the label:
$(document).ready ->
  if $('#ckeditor').length
    CKEDITOR.replace 'ckeditor'
  if $('#ckeditor').prev('label').length
    $('#ckeditor').prev('label').css 'float', 'none'
  $(".datepicker").datepicker();
  $('#active_admin_content .tabs').tabs()
