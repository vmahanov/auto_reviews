class FeedbacksController < ApplicationController
  respond_to :html, :json

  def new
    @feedback = Feedback.new
    respond_modal_with @feedback
  end

  def create
    if (f = params[:feedback])
      @feedback = Feedback.new(
          name:    f[:name],
          email:   f[:email],
          message: f[:message]
      )
      @feedback.save
    end
    respond_modal_with @feedback, location: root_path
  end
end
