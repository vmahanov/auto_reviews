class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_common

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

  private

  def set_common
    unless self.class.parent == Admin # Только если не админка
      @brands = Brand.all
      @popular = AutoShow.where(popular: true).all
    end
  end
end
