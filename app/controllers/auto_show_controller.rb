class AutoShowController < ApplicationController

  before_action :init_review, only: [:index, :index_of_brand, :index_of_best, :index_of_worst, :index_of_discussed, :show]

  def index
    @auto_shows=AutoShow.ordered
    @catalog=Catalog.find_by_href_id('best')
    add_paginate
  end

  def index_of_brand
    @brand = (Brand.find_by_href_id(params[:href_id]) or not_found)
    @auto_shows=AutoShow.brand(@brand.id)
    @catalog=Catalog.find_by_href_id('brands')
    add_paginate
    render 'index'
  end

  def index_of_best
    @auto_shows=AutoShow.best
    @catalog=Catalog.find_by_href_id('best')
    add_paginate
    render 'index'
  end

  def index_of_worst
    @auto_shows=AutoShow.worst
    @catalog=Catalog.find_by_href_id('worst')
    add_paginate
    render 'index'
  end

  def index_of_discussed
    @auto_shows=AutoShow.discussed
    @catalog=Catalog.find_by_href_id('discussed')
    add_paginate
    render 'index'
  end

  def show
    @auto_show = (AutoShow.find_by_href_id(params[:href_id]) or not_found)
  end

  def init_review
    @review = Review.new
  end

  private

  def add_paginate
    @pages = (@auto_shows.count/9.0).ceil
    @page = (params[:page].to_i if params[:page]) || 1
    @page = @pages if @page > @pages
    @auto_shows=@auto_shows.limit(9).offset(9*@page-9)
  end

end
