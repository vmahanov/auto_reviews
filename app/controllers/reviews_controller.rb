class ReviewsController < ApplicationController
  respond_to :html, :json

  def index
    @reviews = Review.ordered.all
  end

  def index_of_last
    @reviews = Review.last_registered.ordered.all
    render 'index'
  end

  def show
  end

  def new
    @review = Review.new
    respond_modal_with @review
  end

  def create
    if (f = params[:review])
      @review = Review.new(
          auto_show_id:f[:auto_show],
          name:        f[:name],
          email:       f[:email],
          description: f[:description],
          value:       f[:value],
          photo:       f[:photo])
      @review.save
    end
    respond_modal_with @review, location: root_path
  end
end
