class CreateAutoShows < ActiveRecord::Migration[5.0]
  def change
    create_table :auto_shows do |t|
      t.string :name
      t.string :address
      t.string :work_time
      t.string :phone
      t.string :site
      t.boolean :popular
      t.string :logo
      t.string :picture
      t.text :services
      t.string :brands
      t.text :variants
      t.text :description
      t.string :href_id

      t.timestamps
    end
  end
end
