class AddEmailToAutoShows < ActiveRecord::Migration[5.0]
  def change
    add_column :auto_shows, :email,      :string
    add_column :auto_shows, :top_header, :string
    add_column :auto_shows, :top_text,   :text

  end
end
