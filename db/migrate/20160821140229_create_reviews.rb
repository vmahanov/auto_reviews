class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.string :name
      t.string :city
      t.string :email
      t.references :auto_show, foreign_key: true
      t.text :description
      t.integer :value
      t.string :photo

      t.timestamps
    end
  end
end
