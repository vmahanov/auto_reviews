class CreateCatalogs < ActiveRecord::Migration[5.0]
  def change
    create_table :catalogs do |t|
      t.string :href_id
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
