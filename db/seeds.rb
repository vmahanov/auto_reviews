# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

brands =[
 ["HYUNDAI","hyundai"],
 ["KIA","kia"],
 ["CHEVROLET","chevrolet"],
 ["OPEL","opel"],
 ["NISSAN","nissan"],
 ["VOLKSWAGEN","volkswagen"],
 ["GREAT WALL","great_wall"],
 ["FORD","ford"],
 ["RENAULT","renault"],
 ["SKODA","skoda"],
 ["MAZDA","mazda"],
 ["TOYOTA","toyota"],
 ["MITSUBISHI","mitsubishi"],
 ["HONDA","honda"],
 ["SUZUKI","suzuki"],
 ["PEUGEOT","peugeot"],
 ["CITROEN","citroen"],
 ["SSANG YONG","ssang_yong"],
 ["LADA","lada"],
 ["GEELY","geely"],
 ["LIFAN","lifan"],
 ["RAVON","ravon"],
 ["CHANGAN","changan"],
 ["DONGFENG","dongfeng"],
 ["DATSUN","datsun"],
 ["BRILLIANCE","brilliance"],
 ["ZOTYE","zotye"],
 ["UAZ","uaz"]
]
brands.each{|name, href_id| Brand.create(name: name, href_id: href_id)}

catalogs=[
    ["best", 'Топ лучшик'],
    ["worst", 'Топ худших'],
    ["discussed", 'Самые обсуждаемые'],
    ["brands", 'Топ продавцов %{brand}']
]
catalogs.each{|href_id,title| Catalog.create(href_id: href_id, title: title, description: '')}