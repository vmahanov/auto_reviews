Rails.application.routes.draw do
  get 'pages/contacts'

  get 'pages/about'

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root                       to: "auto_show#index"
  get 'auto_shows/:href_id', to: "auto_show#show"
  get 'brands/:href_id',     to: "auto_show#index_of_brand"
  get 'best',                to: "auto_show#index_of_best"
  get 'worst',               to: "auto_show#index_of_worst"
  get 'most_discussed',      to: "auto_show#index_of_discussed"

  get 'last_reviews', to: "reviews#index_of_last"
  resources :reviews, only: [:index, :show, :new, :create]
  resources :feedbacks, only: [:new, :create]


  get 'about',    to: "pages#about"
  get 'contacts', to: "pages#contacts"

end
