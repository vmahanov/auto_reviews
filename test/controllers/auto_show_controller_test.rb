require 'test_helper'

class AutoShowControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get auto_show_index_url
    assert_response :success
  end

  test "should get show" do
    get auto_show_show_url
    assert_response :success
  end

end
